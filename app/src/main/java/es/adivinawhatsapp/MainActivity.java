package es.adivinawhatsapp;

import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class MainActivity extends ActionBarActivity {

    private Cursor films;
    private MyDatabase db;
    LinearLayout mLinearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new MyDatabase(this);
        films = db.getFilms(); // you would not typically call this on the main thread

        //El cursor ahora apunta a la primera fila
        // Accedemos a las columnas
        int nameColumnIndex = films.getColumnIndex("code");
        String name = films.getString(nameColumnIndex);


        String[] fileNames = filenamesFromCode(name);

        // Create a LinearLayout in which to add the ImageView
        mLinearLayout = new LinearLayout(this);

        int j=0;
        for(String s: fileNames){

            // Instantiate an ImageView and define its properties
            ImageView i = new ImageView(this);
            i.setImageResource(getApplicationContext().getResources().getIdentifier(fileNames[j],"drawable",getApplicationContext().getPackageName()));
            i.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
            i.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));

            // Add the ImageView to the layout and set the layout as the content view
            mLinearLayout.addView(i);
            j++;
        }

        setContentView(mLinearLayout);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String[] filenamesFromCode(String code){
        String[] ret = new String[code.length()/3];

        int j=0;
        for(int i=0;i<code.length();i+=3){
            ret[j] ="i"+code.substring(i,i+3);
            j++;
        }

        return ret;
    }
}
